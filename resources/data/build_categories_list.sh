#!/usr/bin/env bash

command -v jq >/dev/null 2>&1 || {
  echo >&2 "I require jq (json parser) but it's not installed. Aborting."
  exit 1
}

CURRENT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
BASE_DIR="$(dirname "$(dirname "${CURRENT_DIR}")")"

# CSV source file for categories
SOURCE_CATEGORIES_CSV_FILE="${CURRENT_DIR}/categories.csv"

# Temp json file
OUTPUT_CATEGORIES_JSON_FILE="${CURRENT_DIR}/categories.json"
touch "${OUTPUT_CATEGORIES_JSON_FILE}"

echo "Sorting source csv file..."
cat "${SOURCE_CATEGORIES_CSV_FILE}" | sort | uniq >"${SOURCE_CATEGORIES_CSV_FILE}.tmp"
mv "${SOURCE_CATEGORIES_CSV_FILE}.tmp" "${SOURCE_CATEGORIES_CSV_FILE}"

echo "Building categories json file..."
OUTPUT_CATEGORIES_JSON_FILE_TMP="${OUTPUT_CATEGORIES_JSON_FILE}.tmp"
echo "{" >"${OUTPUT_CATEGORIES_JSON_FILE_TMP}"
echo "  \"categories\": {" >>"${OUTPUT_CATEGORIES_JSON_FILE_TMP}"
FIRST_WORD=1
while read -r LINE; do
  if [[ -n "${LINE}" ]]; then
    echo "- ${LINE}"

    CATEGORY_THEME="$(echo "${LINE}" | cut -d';' -f1)"
    CATEGORY_EMOJIS="$(echo "${LINE}" | cut -d';' -f2)"
    CATEGORY_TEXT="$(echo "${LINE}" | cut -d';' -f3)"
    CATEGORY_TAGS="$(echo "${LINE}" | cut -d';' -f4)"

    CATEGORY_CODE="$(echo "${CATEGORY_TEXT}" | md5sum | awk '{print $1;}')"

    if [[ ${FIRST_WORD} -eq 0 ]]; then
      echo "    ," >>"${OUTPUT_CATEGORIES_JSON_FILE_TMP}"
    fi

    echo "    \"${CATEGORY_CODE}\": {" >>"${OUTPUT_CATEGORIES_JSON_FILE_TMP}"
    echo "      \"theme\": \"${CATEGORY_THEME}\"," >>"${OUTPUT_CATEGORIES_JSON_FILE_TMP}"
    echo "      \"text\": \"${CATEGORY_TEXT}\"," >>"${OUTPUT_CATEGORIES_JSON_FILE_TMP}"
    echo "      \"tags\": [" >>"${OUTPUT_CATEGORIES_JSON_FILE_TMP}"
    if [ ! -z "${CATEGORY_EMOJIS}" ]; then
      echo "        \"${CATEGORY_TAGS}\"" >>"${OUTPUT_CATEGORIES_JSON_FILE_TMP}"
    fi
    echo "      ]," >>"${OUTPUT_CATEGORIES_JSON_FILE_TMP}"
    echo "      \"emojis\": [" >>"${OUTPUT_CATEGORIES_JSON_FILE_TMP}"
    if [ ! -z "${CATEGORY_EMOJIS}" ]; then
      echo "        \"${CATEGORY_EMOJIS}\"" >>"${OUTPUT_CATEGORIES_JSON_FILE_TMP}"
    fi
    echo "      ]" >>"${OUTPUT_CATEGORIES_JSON_FILE_TMP}"
    echo "    }" >>"${OUTPUT_CATEGORIES_JSON_FILE_TMP}"

    FIRST_WORD=0
  fi
done < <(cat "${SOURCE_CATEGORIES_CSV_FILE}")

echo "  }" >>"${OUTPUT_CATEGORIES_JSON_FILE_TMP}"
echo "}" >>"${OUTPUT_CATEGORIES_JSON_FILE_TMP}"

echo "Formatting JSON data file..."

cat "${OUTPUT_CATEGORIES_JSON_FILE_TMP}" | jq >"${OUTPUT_CATEGORIES_JSON_FILE}"
rm "${OUTPUT_CATEGORIES_JSON_FILE_TMP}"

echo "Injecting json file in game_data.dart..."

GAME_DATA_DART_FILE="${BASE_DIR}/lib/data/game_data.dart"
echo "class GameData {" >"${GAME_DATA_DART_FILE}"
echo "  static const Map<String, dynamic> data = $(cat "${OUTPUT_CATEGORIES_JSON_FILE}");" >>"${GAME_DATA_DART_FILE}"
echo "}" >>"${GAME_DATA_DART_FILE}"

echo "Formatting dart source code file..."

dart format "${GAME_DATA_DART_FILE}"

# Delete json temp file
rm "${OUTPUT_CATEGORIES_JSON_FILE}"

echo "done."

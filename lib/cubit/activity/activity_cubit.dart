import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:petitbac/config/application_config.dart';
import 'package:petitbac/data/fetch_data_helper.dart';
import 'package:petitbac/models/activity/activity.dart';
import 'package:petitbac/models/activity/game_item.dart';

part 'activity_state.dart';

class ActivityCubit extends HydratedCubit<ActivityState> {
  ActivityCubit()
      : super(ActivityState(
          currentActivity: Activity.createEmpty(),
        ));

  void updateState(Activity activity) {
    emit(ActivityState(
      currentActivity: activity,
    ));
  }

  void refresh() {
    final Activity activity = Activity(
      // Settings
      activitySettings: state.currentActivity.activitySettings,
      // State
      isRunning: state.currentActivity.isRunning,
      isStarted: state.currentActivity.isStarted,
      isFinished: state.currentActivity.isFinished,
      animationInProgress: state.currentActivity.animationInProgress,
      // Base data
      items: state.currentActivity.items,
      // Game data
      position: state.currentActivity.position,
      countdown: state.currentActivity.countdown,
    );
    // game.dump();

    updateState(activity);
  }

  void startNewActivity(BuildContext context) {
    final ActivitySettingsCubit activitySettingsCubit =
        BlocProvider.of<ActivitySettingsCubit>(context);

    final Activity newActivity = Activity.createNew(
      // Settings
      activitySettings: activitySettingsCubit.state.settings,
    );

    newActivity.dump();

    updateState(newActivity);
    startTimer();
  }

  bool canBeResumed() {
    return state.currentActivity.canBeResumed;
  }

  void quitActivity() {
    state.currentActivity.isRunning = false;
    refresh();
  }

  void resumeSavedActivity() {
    state.currentActivity.isRunning = true;
    refresh();
  }

  void deleteSavedActivity() {
    state.currentActivity.isRunning = false;
    state.currentActivity.isFinished = true;
    refresh();
  }

  void next() {
    if (state.currentActivity.position <
        (state.currentActivity.activitySettings
                .getAsInt(ApplicationConfig.parameterCodeItemsCount) -
            1)) {
      state.currentActivity.position++;
      startTimer();
    } else {
      state.currentActivity.isFinished = true;
    }
    refresh();
  }

  void startTimer() {
    int timerValue = state.currentActivity.activitySettings
        .getAsInt(ApplicationConfig.parameterCodeTimerValue);

    if (timerValue != 0) {
      state.currentActivity.countdown = timerValue;

      const Duration interval = Duration(seconds: 1);
      Timer.periodic(
        interval,
        (Timer timer) {
          if (state.currentActivity.countdown != 0) {
            state.currentActivity.countdown = max(state.currentActivity.countdown - 1, 0);
            refresh();
          }

          if (state.currentActivity.countdown == 0) {
            timer.cancel();
            if (state.currentActivity.position ==
                (state.currentActivity.activitySettings
                        .getAsInt(ApplicationConfig.parameterCodeItemsCount) -
                    1)) {
              state.currentActivity.isFinished = true;
            }
          }

          refresh();
        },
      );
    } else {
      if (state.currentActivity.position ==
          (state.currentActivity.activitySettings
                  .getAsInt(ApplicationConfig.parameterCodeItemsCount) -
              1)) {
        state.currentActivity.isFinished = true;
      }
    }

    refresh();
  }

  void pickNewItem() {
    state.currentActivity.items[state.currentActivity.position] =
        FetchDataHelper().getRandomItem();
    refresh();
  }

  void pickNewCategory() {
    GameItem newItem = GameItem(
      letter: state.currentActivity.letter,
      category: FetchDataHelper().getRandomItem().category,
    );

    state.currentActivity.items[state.currentActivity.position] = newItem;
    refresh();
  }

  void pickNewLetter() {
    GameItem newItem = GameItem(
      letter: FetchDataHelper().getRandomItem().letter,
      category: state.currentActivity.category,
    );

    state.currentActivity.items[state.currentActivity.position] = newItem;
    refresh();
  }

  @override
  ActivityState? fromJson(Map<String, dynamic> json) {
    final Activity currentActivity = json['currentActivity'] as Activity;

    return ActivityState(
      currentActivity: currentActivity,
    );
  }

  @override
  Map<String, dynamic>? toJson(ActivityState state) {
    return <String, dynamic>{
      'currentActivity': state.currentActivity.toJson(),
    };
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:petitbac/cubit/activity/activity_cubit.dart';

import 'package:petitbac/ui/pages/game.dart';

class ApplicationConfig {
  // activity parameter: items count
  static const String parameterCodeItemsCount = 'activity.itemsCount';
  static const String itemsCountValueNoLimit = 'nolimit';
  static const String itemsCountValueShort = 'short';
  static const String itemsCountValueMedium = 'medium';
  static const String itemsCountValueLong = 'long';

  // activity parameter: timer values
  static const String parameterCodeTimerValue = 'activity.timerValue';
  static const String timerValueNoTimer = 'nolimit';
  static const String timerValueLow = 'low';
  static const String timerValueMedium = 'medium';
  static const String timerValueHigh = 'high';

  // activity pages
  static const int activityPageIndexHome = 0;
  static const int activityPageIndexGame = 1;

  static final ApplicationConfigDefinition config = ApplicationConfigDefinition(
    appTitle: 'Petit Bac',
    activitySettings: [
      // items count
      ApplicationSettingsParameter(
        code: parameterCodeItemsCount,
        values: [
          ApplicationSettingsParameterItemValue(
            value: itemsCountValueNoLimit,
            color: Colors.grey,
            text: '⭐',
          ),
          ApplicationSettingsParameterItemValue(
            value: itemsCountValueShort,
            color: Colors.green,
            text: '💬 5',
          ),
          ApplicationSettingsParameterItemValue(
            value: itemsCountValueMedium,
            color: Colors.orange,
            text: '💬 10',
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: itemsCountValueLong,
            color: Colors.red,
            text: '💬 20',
          ),
        ],
        intValueGetter: (value) {
          const Map<String, int> intValues = {
            itemsCountValueNoLimit: 0,
            itemsCountValueShort: 5,
            itemsCountValueMedium: 10,
            itemsCountValueLong: 20,
          };
          return intValues[value] ?? 0;
        },
      ),

      // timer value
      ApplicationSettingsParameter(
        code: parameterCodeTimerValue,
        values: [
          ApplicationSettingsParameterItemValue(
            value: timerValueNoTimer,
            color: Colors.grey,
            text: '⭐',
          ),
          ApplicationSettingsParameterItemValue(
            value: timerValueLow,
            color: Colors.green,
            text: '⏲️ 5',
          ),
          ApplicationSettingsParameterItemValue(
            value: timerValueMedium,
            color: Colors.orange,
            text: '⏲️ 30',
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: timerValueHigh,
            color: Colors.red,
            text: '⏲️ 90',
          ),
        ],
        intValueGetter: (value) {
          const Map<String, int> intValues = {
            timerValueNoTimer: 0,
            timerValueLow: 5,
            timerValueMedium: 30,
            timerValueHigh: 90,
          };
          return intValues[value] ?? 0;
        },
      ),
    ],
    startNewActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).startNewActivity(context);
      BlocProvider.of<NavCubitPage>(context).updateIndex(activityPageIndexGame);
    },
    quitCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).quitActivity();
      BlocProvider.of<NavCubitPage>(context).updateIndex(activityPageIndexHome);
    },
    deleteCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).deleteSavedActivity();
    },
    resumeActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).resumeSavedActivity();
      BlocProvider.of<NavCubitPage>(context).updateIndex(activityPageIndexGame);
    },
    navigation: ApplicationNavigation(
      screenActivity: ScreenItem(
        code: 'screen_activity',
        icon: Icon(UniconsLine.home),
        screen: ({required ApplicationConfigDefinition appConfig}) =>
            ScreenActivity(appConfig: appConfig),
      ),
      screenSettings: ScreenItem(
        code: 'screen_settings',
        icon: Icon(UniconsLine.setting),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenSettings(),
      ),
      screenAbout: ScreenItem(
        code: 'screen_about',
        icon: Icon(UniconsLine.info_circle),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenAbout(),
      ),
      activityPages: {
        activityPageIndexHome: ActivityPageItem(
          code: 'page_home',
          icon: Icon(UniconsLine.home),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageParameters(
                  appConfig: appConfig,
                  canBeResumed: activityState.currentActivity.canBeResumed,
                );
              },
            );
          },
        ),
        activityPageIndexGame: ActivityPageItem(
          code: 'page_game',
          icon: Icon(UniconsLine.star),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageGame();
              },
            );
          },
        ),
      },
    ),
  );
}

class GameData {
  static const Map<String, dynamic> data = {
    "categories": {
      "0b22bcbee14d4a271e06aba366f13685": {
        "theme": "BASE",
        "text": "Chose ou objet",
        "tags": ["*"],
        "emojis": ["📦"]
      },
      "411b6e48f3f62f6d0a2f621945811c5a": {
        "theme": "BASE",
        "text": "Couleur",
        "tags": ["*"],
        "emojis": ["🎨"]
      },
      "8dbc4f0142ace2221e0cee2e7e136773": {
        "theme": "BASE",
        "text": "Élément de véhicules",
        "tags": ["*"],
        "emojis": ["🚗"]
      },
      "7c6267a89252e2a86c7b556d6ee9eed4": {
        "theme": "BASE",
        "text": "Fleur",
        "tags": ["*"],
        "emojis": ["💐"]
      },
      "e5512d1ae2a2f7cf6cfe2c920045b64f": {
        "theme": "BASE",
        "text": "Moyen de transport",
        "tags": ["*"],
        "emojis": ["🚂"]
      },
      "3daa9627db8176b1960aecc4c308f77f": {
        "theme": "BASE",
        "text": "Objet du quotidien",
        "tags": ["*"],
        "emojis": ["🥣"]
      },
      "57402276e16b33ddd9608d2ecd2c2a78": {
        "theme": "BASE",
        "text": "Outil (de bricolage)",
        "tags": ["*"],
        "emojis": ["🛠️"]
      },
      "1e6a85c6fab111dca1d0d92927bd5688": {
        "theme": "BASE",
        "text": "Qualité ou défaut",
        "tags": ["*"],
        "emojis": ["🙃"]
      },
      "0094e189fc88b62c25efe16d82ccc649": {
        "theme": "BASE",
        "text": "Site internet",
        "tags": ["*"],
        "emojis": ["💻"]
      },
      "c1c2df686456aca0b8b731ff0ff0e55d": {
        "theme": "BASE",
        "text": "Marque",
        "tags": ["*"],
        "emojis": ["™️"]
      },
      "e7211f646b121c642d61b359f356215d": {
        "theme": "BASE",
        "text": "Vêtement",
        "tags": ["*"],
        "emojis": ["👖"]
      },
      "1a6dab4b49fc4d0aadd636beff6147fb": {
        "theme": "CARACTERISTIQUE",
        "text": "Mauvais pour la santé",
        "tags": ["*"],
        "emojis": ["🤢"]
      },
      "bba10106ffd5b8871fda534dab1ce370": {
        "theme": "CARACTERISTIQUE",
        "text": "Mauvais pour l'environement",
        "tags": ["*"],
        "emojis": ["🏭"]
      },
      "2343e346c1daf53911b5f9bac55b0995": {
        "theme": "CARACTERISTIQUE",
        "text": "Quelque chose de pointu",
        "tags": ["*"],
        "emojis": ["📌"]
      },
      "3a8da538fb29c9eeaa875cbefec3d6e1": {
        "theme": "CARACTERISTIQUE",
        "text": "Quelque chose de rond",
        "tags": ["*"],
        "emojis": ["⚽"]
      },
      "682172213e6a2c36c21987ba0de89a76": {
        "theme": "CARACTERISTIQUE",
        "text": "Quelque chose de vieux",
        "tags": ["*"],
        "emojis": ["🏚️"]
      },
      "87da4a342bf6b4b04c650c169d042160": {
        "theme": "CARACTERISTIQUE",
        "text": "Quelque chose qui coule",
        "tags": ["*"],
        "emojis": ["🪨"]
      },
      "710d0bcfd792c882bac0d2b1df96adba": {
        "theme": "CARACTERISTIQUE",
        "text": "Quelque chose qui flotte",
        "tags": ["*"],
        "emojis": ["🪵"]
      },
      "6f0347c47216eee873abaf2eee9b1bdf": {
        "theme": "CARACTERISTIQUE",
        "text": "Quelque chose qui vole",
        "tags": ["*"],
        "emojis": ["🪁"]
      },
      "15b126df81118cb49d53ae890c9422cd": {
        "theme": "CARACTERISTIQUE",
        "text": "Qui fait peur",
        "tags": ["*"],
        "emojis": ["🙀"]
      },
      "353244d5a0edc864bca0248f61bbf61f": {
        "theme": "CARACTERISTIQUE",
        "text": "Qui fait plaisir",
        "tags": ["*"],
        "emojis": ["🥰"]
      },
      "d01573193aeae6bba92c404523b31314": {
        "theme": "CARACTERISTIQUE",
        "text": "Qui gratte",
        "tags": ["*"],
        "emojis": ["🪥"]
      },
      "7ab3bcc870023bf84d9e31f649422b39": {
        "theme": "CARACTERISTIQUE",
        "text": "Qui n'est pas bon",
        "tags": ["*"],
        "emojis": ["🤮"]
      },
      "6986abaceaa67997c71d608aeda22883": {
        "theme": "CARACTERISTIQUE",
        "text": "Qui sent mauvais",
        "tags": ["*"],
        "emojis": ["💩"]
      },
      "073cfc7cbb1dc67d4558c57efda05668": {
        "theme": "COULEUR",
        "text": "Quelque chose de blanc",
        "tags": ["*"],
        "emojis": ["⬜"]
      },
      "153523a555044ac1c8cc0cdda9c9e830": {
        "theme": "COULEUR",
        "text": "Quelque chose de bleu",
        "tags": ["*"],
        "emojis": ["🟦"]
      },
      "9565bc0ff9bd2ac4353b56aa4e14dbcc": {
        "theme": "COULEUR",
        "text": "Quelque chose de jaune",
        "tags": ["*"],
        "emojis": ["🟨"]
      },
      "bef258780165a9f30cbd635ac9e67c04": {
        "theme": "COULEUR",
        "text": "Quelque chose de rouge",
        "tags": ["*"],
        "emojis": ["🟥"]
      },
      "e1841faafea47999bb8c8f63129e5a4c": {
        "theme": "COULEUR",
        "text": "Quelque chose de vert",
        "tags": ["*"],
        "emojis": ["🟩"]
      },
      "2637a1ba20569131f55ef2f02e34745b": {
        "theme": "CULTURE",
        "text": "Acteur ou actrice",
        "tags": ["*"],
        "emojis": ["🎭"]
      },
      "8fc2c3e7966f76969425ad6034beced8": {
        "theme": "CULTURE",
        "text": "Auteur de littérature",
        "tags": ["*"],
        "emojis": ["🧑‍🏫"]
      },
      "e1c830f04fe8bcad898cb26ca542eb9a": {
        "theme": "CULTURE",
        "text": "Chanteur ou chanteuse",
        "tags": ["*"],
        "emojis": ["🎤"]
      },
      "0e6964fc00d1e928120d08a760758057": {
        "theme": "CULTURE",
        "text": "Élément chimique",
        "tags": ["*"],
        "emojis": ["🧪"]
      },
      "9bb6871acbd18f1bfc4c3aba3ed47033": {
        "theme": "CULTURE",
        "text": "Femme ou homme politique",
        "tags": ["*"],
        "emojis": ["🧑‍💼"]
      },
      "e6e93a1cb7143124cb2da203bdb7fbf0": {
        "theme": "CULTURE",
        "text": "Film",
        "tags": ["*"],
        "emojis": ["🎥"]
      },
      "a9b705bc593514fad9a5eab5427d1223": {
        "theme": "CULTURE",
        "text": "Héros de mythologie",
        "tags": ["*"],
        "emojis": ["🧞‍♂️"]
      },
      "49c4b018aacf61075f58ed8d398d2ad9": {
        "theme": "CULTURE",
        "text": "Héros fictif",
        "tags": ["*"],
        "emojis": ["🦸"]
      },
      "b84e4c7bc9cc9c5ec0ce691eb4b3d92b": {
        "theme": "CULTURE",
        "text": "Instrument de musique",
        "tags": ["*"],
        "emojis": ["🎸"]
      },
      "030a2cda35509bf1a20da88dfd39d3e6": {
        "theme": "CULTURE",
        "text": "Jeu (de société)",
        "tags": ["*"],
        "emojis": ["🎲"]
      },
      "8522ab027797ee51bb240723b9db9c8a": {
        "theme": "CULTURE",
        "text": "Métal",
        "tags": ["*"],
        "emojis": ["🖇️"]
      },
      "33714cab576b9f84232c97bca6f7512a": {
        "theme": "CULTURE",
        "text": "Personnage de bande dessinée",
        "tags": ["*"],
        "emojis": ["🗯️"]
      },
      "2151287317faaee4c18e80417f5a39cf": {
        "theme": "CULTURE",
        "text": "Personnage de dessin animé",
        "tags": ["*"],
        "emojis": ["🧜‍♀️"]
      },
      "69146d0c1cfd909833a15f835c9c3916": {
        "theme": "CULTURE",
        "text": "Sport",
        "tags": ["*"],
        "emojis": ["🏅"]
      },
      "7751f036804d33b28ea74a25b481f14e": {
        "theme": "CULTURE",
        "text": "Titre de livre",
        "tags": ["*"],
        "emojis": ["📘"]
      },
      "33d08cd2613be7886b47296eda96c096": {
        "theme": "DICTIONNAIRE",
        "text": "Mot de plus de 8 lettres",
        "tags": ["*"],
        "emojis": ["8️⃣"]
      },
      "f7cbd58d06982f902ecf8e5d559d1aee": {
        "theme": "DICTIONNAIRE",
        "text": "Mot de 4 lettres",
        "tags": ["*"],
        "emojis": ["🩻"]
      },
      "0421d3a88fe6af3c95fe83aae7b0eb75": {
        "theme": "GEOGRAPHIE",
        "text": "Capitale",
        "tags": ["*"],
        "emojis": ["🗽"]
      },
      "fb0573523fb9896af3cdf07dc8ffec36": {
        "theme": "GEOGRAPHIE",
        "text": "Département français",
        "tags": ["*"],
        "emojis": ["🇫🇷"]
      },
      "4353f024f71dbc7ab329258e28b7cbdb": {
        "theme": "GEOGRAPHIE",
        "text": "Pays",
        "tags": ["*"],
        "emojis": ["🗺️"]
      },
      "ae72d1e8f030223951aa72e563238674": {
        "theme": "GEOGRAPHIE",
        "text": "Ville",
        "tags": ["*"],
        "emojis": ["🏙️"]
      },
      "839f7d50ac155a668e89bf2397c94e14": {
        "theme": "GEOGRAPHIE",
        "text": "Ville française",
        "tags": ["*"],
        "emojis": ["🇫🇷"]
      },
      "38582ab90e57f7ebfa274c404c9d229f": {
        "theme": "HUMAIN",
        "text": "Métier",
        "tags": ["*"],
        "emojis": ["👷"]
      },
      "774dab0b1b389961e552f1d023221b54": {
        "theme": "HUMAIN",
        "text": "Métier dont rêvent les enfants",
        "tags": ["*"],
        "emojis": ["🧑‍⚕️"]
      },
      "0011593bec14f0385250adb8e130ba6c": {
        "theme": "HUMAIN",
        "text": "Partie du corps humain",
        "tags": ["*"],
        "emojis": ["🖐️"]
      },
      "cda8dfc4f4bea88d980cac9b25e8a303": {
        "theme": "HUMAIN",
        "text": "Personnage historique",
        "tags": ["*"],
        "emojis": ["🤴"]
      },
      "f464eeecbba4108a370232240ede38c5": {
        "theme": "HUMAIN",
        "text": "Prénom fille",
        "tags": ["*"],
        "emojis": ["👧🏼"]
      },
      "0652b2299465b642452ee0a87191aaa1": {
        "theme": "HUMAIN",
        "text": "Prénom garçon",
        "tags": ["*"],
        "emojis": ["👦🏼"]
      },
      "5a8a3e525551da2fde595be796932c07": {
        "theme": "INSOLITE",
        "text": "Bruit ou son",
        "tags": ["*"],
        "emojis": ["🔔"]
      },
      "7856436970584c92a60523bdcd7a1371": {
        "theme": "INSOLITE",
        "text": "Cadeau de Noël ou d'anniversaire",
        "tags": ["*"],
        "emojis": ["🎁"]
      },
      "47e6a5854bb1cbe7a8acf7cc52052cb1": {
        "theme": "INSOLITE",
        "text": "Cadeau nul",
        "tags": ["*"],
        "emojis": ["🎁"]
      },
      "e82dbaa90f7c27df45bd0cfa47aea19a": {
        "theme": "INSOLITE",
        "text": "Injure originale",
        "tags": ["*"],
        "emojis": ["🖕"]
      },
      "c22afed1477a8b43c1685b1c105aa62e": {
        "theme": "INSOLITE",
        "text": "Lieu incontournable",
        "tags": ["*"],
        "emojis": ["📍"]
      },
      "3b014cd0032268414d042474232f4ae0": {
        "theme": "INSOLITE",
        "text": "Mot qui se termine en -ard",
        "tags": ["*"],
        "emojis": ["🗣️"]
      },
      "d198a358a51b4e5682ff6997e875e9a0": {
        "theme": "INSOLITE",
        "text": "Mot qui se termine en -asse",
        "tags": ["*"],
        "emojis": ["🗣️"]
      },
      "e19b809fe6a3de12bfe4a0ecf79855aa": {
        "theme": "INSOLITE",
        "text": "Mots qui riment avec ouille",
        "tags": ["*"],
        "emojis": ["🗣️"]
      },
      "fe33ed5273b5348cfa449e49ef8d2383": {
        "theme": "INSOLITE",
        "text": "Objet insolite",
        "tags": ["*"],
        "emojis": ["🃏"]
      },
      "2d8f23360134fb1498386fd27618d978": {
        "theme": "INSOLITE",
        "text": "On peut y planquer un corps",
        "tags": ["*"],
        "emojis": ["🪦"]
      },
      "e6d6340bc92a2469998dcc7ddc1449d4": {
        "theme": "INSOLITE",
        "text": "Un gros mot",
        "tags": ["*"],
        "emojis": ["🤬"]
      },
      "36e7748ac773bbe1b8b7d6a7eef75362": {
        "theme": "INSOLITE",
        "text": "Un truc moche",
        "tags": ["*"],
        "emojis": ["🥴"]
      },
      "fdebc26184afa53da6d1f996a8bbf780": {
        "theme": "LIEU",
        "text": "À la plage",
        "tags": ["*"],
        "emojis": ["🏖️"]
      },
      "87b930d71b3154e8ba5d8cf83d91d3ca": {
        "theme": "LIEU",
        "text": "Dans la mer",
        "tags": ["*"],
        "emojis": ["🌊"]
      },
      "90ca0104660b01bdd58b694e187066b8": {
        "theme": "LIEU",
        "text": "Dans sa poche",
        "tags": ["*"],
        "emojis": ["👝"]
      },
      "e2f6f6eb9c93cb6c64511ae674dabc0c": {
        "theme": "LIEU",
        "text": "Dans un camping",
        "tags": ["*"],
        "emojis": ["🏕️"]
      },
      "f22d95afbd5d0db4d8a546ddb4d63fad": {
        "theme": "LIEU",
        "text": "Dans un cartable",
        "tags": ["*"],
        "emojis": ["🎒"]
      },
      "010e8e426c11191dc5a8fbf439de6784": {
        "theme": "LIEU",
        "text": "Dans une forêt",
        "tags": ["*"],
        "emojis": ["🌲"]
      },
      "e894a19da5d9e87b882b3551d4cccab6": {
        "theme": "LIEU",
        "text": "Dans une maison",
        "tags": ["*"],
        "emojis": ["🏠"]
      },
      "12c64951be30a4b0b02df2099d51dfbb": {
        "theme": "LIEU",
        "text": "Dans une voiture",
        "tags": ["*"],
        "emojis": ["🚙"]
      },
      "239f150afef0e4bca48148a1eb1c9419": {
        "theme": "LIEU",
        "text": "Dans un frigo",
        "tags": ["*"],
        "emojis": ["🧊"]
      },
      "0e43f680ef179f16bc83cf191fa8b3ee": {
        "theme": "LIEU",
        "text": "Dans un sac à main",
        "tags": ["*"],
        "emojis": ["👜"]
      },
      "e2f66a529e0c1c0eb607aece83899540": {
        "theme": "MONDE",
        "text": "Mot en anglais",
        "tags": ["*"],
        "emojis": ["🇬🇧"]
      },
      "daa6fb5434acfef7c1918f14118eaa1e": {
        "theme": "MONDE",
        "text": "Mot en espagnol",
        "tags": ["*"],
        "emojis": ["🇪🇸"]
      },
      "c0adb8b36a2c55a5a6b408aebb796e25": {
        "theme": "NATURE",
        "text": "Animal",
        "tags": ["*"],
        "emojis": ["🐒"]
      },
      "06604f3c64bad81c7146b36b358f34e6": {
        "theme": "NATURE",
        "text": "Arbre",
        "tags": ["*"],
        "emojis": ["🌳"]
      },
      "3fafbf004569d2859d1f98ceaeda0381": {
        "theme": "NATURE",
        "text": "Dans l’univers (étoile, planète, constellation, ...)",
        "tags": ["*"],
        "emojis": ["🪐"]
      },
      "6e080db66b45daa1b9acdefd075ce06d": {
        "theme": "NATURE",
        "text": "Fleuve, rivière, lac, mer ou océan",
        "tags": ["*"],
        "emojis": ["🌊"]
      },
      "922ef40fdc2bf07dba7b5eb4e392dca5": {
        "theme": "NATURE",
        "text": "Fruit ou légume",
        "tags": ["*"],
        "emojis": ["🍅"]
      },
      "fa064927d7c28e02f8bbff993662ef6c": {
        "theme": "NATURE",
        "text": "Insecte",
        "tags": ["*"],
        "emojis": ["🐞"]
      },
      "6d31fdbabd52ce77d14791e3b6bdba9f": {
        "theme": "NATURE",
        "text": "Mammifère",
        "tags": ["*"],
        "emojis": ["🐇"]
      },
      "72fd000068794c5376ae64770239916f": {
        "theme": "NATURE",
        "text": "Minéral ou pierre précieuse",
        "tags": ["*"],
        "emojis": ["💎"]
      },
      "2eb4e8d37b98f9b59e90604dfe37fd70": {
        "theme": "NATURE",
        "text": "Oiseau",
        "tags": ["*"],
        "emojis": ["🐦"]
      },
      "b7e246847fffd2e2f6879154bd695dae": {
        "theme": "NATURE",
        "text": "Poisson",
        "tags": ["*"],
        "emojis": ["🐟"]
      },
      "c1ec620d1225c53044199b43faadd2c7": {
        "theme": "NOURRITURE",
        "text": "Boisson",
        "tags": ["*"],
        "emojis": ["🥤"]
      },
      "e074313e9c03900f72578b267f96f15f": {
        "theme": "NOURRITURE",
        "text": "Dessert",
        "tags": ["*"],
        "emojis": ["🍰"]
      },
      "33382e9abddbcf023305489adef8740a": {
        "theme": "NOURRITURE",
        "text": "Épice / Herbe aromatique",
        "tags": ["*"],
        "emojis": ["🌶️"]
      },
      "42e1524ebd7d9836c3cd8507a8306574": {
        "theme": "NOURRITURE",
        "text": "Fromage",
        "tags": ["*"],
        "emojis": ["🧀"]
      },
      "8b44cba9e0879d6da17f99a972606485": {
        "theme": "NOURRITURE",
        "text": "Plat",
        "tags": ["*"],
        "emojis": ["🍲"]
      }
    }
  };
}

import 'dart:math';

import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:petitbac/data/game_data.dart';
import 'package:petitbac/models/data/category.dart';
import 'package:petitbac/models/data/letter.dart';
import 'package:petitbac/models/activity/game_item.dart';

class FetchDataHelper {
  FetchDataHelper();

  final List<Category> _categories = [];
  List<Category> get categories => _categories;

  final List<Letter> _letters = [];
  List<Letter> get letters => _letters;

  void init() {
    try {
      final Map<String, dynamic> rawCategories =
          GameData.data['categories'] as Map<String, dynamic>;
      rawCategories.forEach((categoryCode, categoryRawData) {
        final categoryText = categoryRawData['text'].toString();
        final rawEmojis = categoryRawData['emojis'] as List<dynamic>;

        final categoryEmojis = [];
        for (var emoji in rawEmojis) {
          categoryEmojis.add(emoji);
        }
        _categories.add(Category(
          key: categoryCode,
          text: categoryText,
          emoji: categoryEmojis.first,
        ));
      });
    } catch (e) {
      printlog("$e");
    }

    const String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    for (var char in chars.split('')) {
      _letters.add(Letter(key: char, text: char));
    }
  }

  List<GameItem> getRandomItems(int count) {
    if (_categories.isEmpty || _letters.isEmpty) {
      init();
    }

    // will pick at least one item
    int realCount = max(count, 1);

    List<Category> categories = _categories;
    categories.shuffle();

    List<Letter> letters = _letters;
    letters.shuffle();

    List<GameItem> items = [];
    for (var i = 0; i < realCount; i++) {
      items.add(GameItem(
        letter: letters.elementAt(i),
        category: categories.elementAt(i),
      ));
    }

    return items;
  }

  GameItem getRandomItem() {
    return getRandomItems(1).first;
  }
}

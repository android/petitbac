import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:petitbac/config/application_config.dart';
import 'package:petitbac/data/fetch_data_helper.dart';
import 'package:petitbac/models/data/category.dart';
import 'package:petitbac/models/data/letter.dart';
import 'package:petitbac/models/activity/game_item.dart';

class Activity {
  Activity({
    // Settings
    required this.activitySettings,

    // State
    this.isRunning = false,
    this.isStarted = false,
    this.isFinished = false,
    this.animationInProgress = false,

    // Base data
    required this.items,

    // Game data
    this.position = 0,
    this.countdown = 0,
  });

  // Settings
  final ActivitySettings activitySettings;

  // State
  bool isRunning;
  bool isStarted;
  bool isFinished;
  bool animationInProgress;

  // Base data
  final List<GameItem> items;

  // Game data
  int position;
  int countdown;

  factory Activity.createEmpty() {
    return Activity(
      // Settings
      activitySettings: ActivitySettings.createDefault(appConfig: ApplicationConfig.config),
      // Base data
      items: [],
    );
  }

  factory Activity.createNew({
    ActivitySettings? activitySettings,
  }) {
    final ActivitySettings newActivitySettings = activitySettings ??
        ActivitySettings.createDefault(appConfig: ApplicationConfig.config);

    final List<GameItem> items = FetchDataHelper().getRandomItems(
        newActivitySettings.getAsInt(ApplicationConfig.parameterCodeItemsCount));

    return Activity(
      // Settings
      activitySettings: newActivitySettings,
      // State
      isRunning: true,
      isStarted: true,
      // Base data
      items: items,
    );
  }

  bool get canBeResumed => isStarted && !isFinished;

  GameItem get item => items[position];
  Category get category => item.category;
  Letter get letter => item.letter;

  void dump() {
    printlog('');
    printlog('## Current game dump:');
    printlog('');
    printlog('$Activity:');
    printlog('  Settings');
    activitySettings.dump();
    printlog('  State');
    printlog('    isRunning: $isRunning');
    printlog('    isStarted: $isStarted');
    printlog('    isFinished: $isFinished');
    printlog('    animationInProgress: $animationInProgress');
    printlog('  Base data');
    printlog('    items: $items');
    printlog('  Game data');
    printlog('    position: $position');
    printlog('    countdown: $countdown');
    printlog('');
  }

  @override
  String toString() {
    return '$Activity(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      // Settings
      'activitySettings': activitySettings.toJson(),
      // State
      'isRunning': isRunning,
      'isStarted': isStarted,
      'isFinished': isFinished,
      'animationInProgress': animationInProgress,
      // Base data
      'items': items,
      // Game data
      'position': position,
      'countdown': countdown,
    };
  }
}

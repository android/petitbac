import 'package:petitbac/models/data/category.dart';
import 'package:petitbac/models/data/letter.dart';

class GameItem {
  final Letter letter;
  final Category category;

  GameItem({
    required this.letter,
    required this.category,
  });

  @override
  String toString() {
    return '$GameItem(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      'letter': letter.toJson(),
      'category': category.toJson(),
    };
  }
}

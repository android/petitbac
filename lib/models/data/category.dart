class Category {
  final String key;
  final String text;
  final String emoji;

  const Category({
    required this.key,
    required this.text,
    required this.emoji,
  });

  @override
  String toString() {
    return '$Category(${toJson()})';
  }

  Map<String, dynamic> toJson() {
    return {
      'key': key,
      'text': text,
      'emoji': emoji,
    };
  }
}

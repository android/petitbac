class Letter {
  final String key;
  final String text;

  const Letter({
    required this.key,
    required this.text,
  });

  @override
  String toString() {
    return '$Letter(${toJson()})';
  }

  Map<String, dynamic> toJson() {
    return {
      'key': key,
      'text': text,
    };
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:petitbac/config/application_config.dart';

import 'package:petitbac/cubit/activity/activity_cubit.dart';
import 'package:petitbac/models/activity/activity.dart';
import 'package:petitbac/ui/widgets/game/game_position_indicator.dart';

class GameTopWidget extends StatelessWidget {
  const GameTopWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        return currentActivity.activitySettings
                    .getAsInt(ApplicationConfig.parameterCodeItemsCount) !=
                0
            ? const GamePositionIndicator()
            : const SizedBox.shrink();
      },
    );
  }
}

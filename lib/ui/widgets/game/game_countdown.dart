import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:petitbac/config/application_config.dart';
import 'package:petitbac/cubit/activity/activity_cubit.dart';
import 'package:petitbac/models/activity/activity.dart';

class GameButtonNextWithCountdown extends StatelessWidget {
  const GameButtonNextWithCountdown({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        const Color backgroundColor = Colors.blue;
        const double borderWidth = 8.0;
        final Color borderColor = backgroundColor.darken();

        const Color textColor = Colors.black;

        return BlocBuilder<ActivitySettingsCubit, ActivitySettingsState>(
          builder: (context, settingsSate) {
            return Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(height: 5),
                Container(
                  margin: const EdgeInsets.all(borderWidth),
                  padding: const EdgeInsets.all(borderWidth),
                  decoration: BoxDecoration(
                    color: backgroundColor,
                    borderRadius: BorderRadius.circular(borderWidth),
                    border: Border.all(
                      color: borderColor,
                      width: borderWidth,
                    ),
                  ),
                  child: SizedBox.square(
                    dimension: 100,
                    child: TextButton(
                      onPressed: () {
                        final ActivityCubit activityCubit =
                            BlocProvider.of<ActivityCubit>(context);

                        if (currentActivity.isFinished) {
                          activityCubit.quitActivity();
                        } else {
                          if (currentActivity.activitySettings
                                  .getAsInt(ApplicationConfig.parameterCodeItemsCount) ==
                              0) {
                            activityCubit.pickNewItem();
                            activityCubit.startTimer();
                          } else {
                            if (currentActivity.countdown == 0) {
                              activityCubit.next();
                            }
                          }
                        }
                      },
                      child: Text(
                        currentActivity.isFinished
                            ? '🎆'
                            : ((currentActivity.countdown == 0)
                                ? '🎲'
                                : currentActivity.countdown.toString()),
                        textAlign: TextAlign.center,
                        style: const TextStyle(
                          fontSize: 50,
                          fontWeight: FontWeight.w600,
                          color: textColor,
                        ),
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 5),
              ],
            );
          },
        );
      },
    );
  }
}

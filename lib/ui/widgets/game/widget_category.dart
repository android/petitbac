import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:petitbac/cubit/activity/activity_cubit.dart';
import 'package:petitbac/models/activity/activity.dart';

class WidgetCategory extends StatelessWidget {
  const WidgetCategory({super.key});

  @override
  Widget build(BuildContext context) {
    const Color backgroundColor = Colors.green;
    const double borderWidth = 8.0;

    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        final Color borderColor = backgroundColor.darken();

        return Container(
          width: double.maxFinite,
          margin: const EdgeInsets.all(borderWidth),
          padding: const EdgeInsets.all(borderWidth),
          decoration: BoxDecoration(
            color: backgroundColor,
            borderRadius: BorderRadius.circular(borderWidth),
            border: Border.all(
              color: borderColor,
              width: borderWidth,
            ),
          ),
          child: SizedBox(
            height: 180,
            child: TextButton(
              onPressed: () {
                BlocProvider.of<ActivityCubit>(context).pickNewCategory();
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    currentActivity.category.text,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      fontSize: 35,
                      fontWeight: FontWeight.w600,
                      color: Colors.black,
                      height: 1,
                    ),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Text(
                    currentActivity.category.emoji,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                      fontSize: 50,
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:petitbac/cubit/activity/activity_cubit.dart';
import 'package:petitbac/models/activity/activity.dart';

class WidgetLetter extends StatelessWidget {
  const WidgetLetter({super.key});

  @override
  Widget build(BuildContext context) {
    const Color backgroundColor = Colors.orange;
    const double borderWidth = 8.0;

    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        final Color borderColor = backgroundColor.darken();

        return Container(
          width: double.maxFinite,
          margin: const EdgeInsets.all(borderWidth),
          padding: const EdgeInsets.all(borderWidth),
          decoration: BoxDecoration(
            color: backgroundColor,
            borderRadius: BorderRadius.circular(borderWidth),
            border: Border.all(
              color: borderColor,
              width: borderWidth,
            ),
          ),
          child: SizedBox(
            height: 100,
            child: TextButton(
              onPressed: () {
                BlocProvider.of<ActivityCubit>(context).pickNewLetter();
              },
              child: OutlinedText(
                text: currentActivity.letter.text,
                fontSize: 60,
                textColor: Colors.black,
                outlineColor: Colors.orange.darken(),
              ),
            ),
          ),
        );
      },
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:petitbac/config/application_config.dart';

import 'package:petitbac/cubit/activity/activity_cubit.dart';
import 'package:petitbac/models/activity/activity.dart';

class GamePositionIndicator extends StatelessWidget {
  const GamePositionIndicator({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        final int currentPosition = currentActivity.position + 1;
        final int maxPosition = currentActivity.activitySettings
            .getAsInt(ApplicationConfig.parameterCodeItemsCount);

        // Normalized [0..1] value
        final double barValue = currentPosition / maxPosition;

        const barHeight = 30.0;
        const Color baseColor = Colors.grey;
        const Color textColor = Color.fromARGB(255, 238, 238, 238);
        const Color outlineColor = Color.fromARGB(255, 200, 200, 200);

        return Stack(
          alignment: Alignment.center,
          children: [
            LinearProgressIndicator(
              value: barValue,
              color: baseColor,
              backgroundColor: baseColor.darken(),
              minHeight: barHeight,
              borderRadius: const BorderRadius.all(Radius.circular(10)),
            ),
            OutlinedText(
              text: '$currentPosition/$maxPosition',
              fontSize: barHeight * 0.7,
              textColor: textColor,
              outlineColor: outlineColor,
            ),
          ],
        );
      },
    );
  }
}

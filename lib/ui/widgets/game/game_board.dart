import 'package:flutter/material.dart';

import 'package:petitbac/ui/widgets/game/game_countdown.dart';
import 'package:petitbac/ui/widgets/game/widget_category.dart';
import 'package:petitbac/ui/widgets/game/widget_letter.dart';

class GameBoardWidget extends StatelessWidget {
  const GameBoardWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return const Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        WidgetLetter(),
        GameButtonNextWithCountdown(),
        WidgetCategory(),
      ],
    );
  }
}
